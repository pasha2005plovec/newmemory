﻿#include <iostream>
#include <stack>


    int main()
    {

        const int N = 6;
        struct Node {
            int* A = new int[N];
            int cap = N;
            int count = 0;
        };
        void push(Node* p, int d)
        {

            if (p->count == p->cap)
            {
                const int cap = p->cap * 2;
                int* arr = new int[cap];
                std::move(p->A, p->A + p->count, arr);
                delete[] p->A;
                p->A = arr;
                p->cap = cap;
            }
            p->A[p->count] = d;
            p->count++;
        }
      
    }

